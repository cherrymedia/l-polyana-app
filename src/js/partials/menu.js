var menu = {
    top: document.getElementById('menu-top'),
    left: document.getElementById('menu-left'),
    content: document.getElementById('content'),
    menu: [],
    itemList: [],
    cart: {
        items: {},
        pizza: {
            items: {},
            add: function (id, count) {
                if (this.count() == 4 || this.items[id] == 3)
                    return;

                var free = 4 - this.count();

                if (free > 0)
                    if (!!this.items[id]) {
                        if (this.items[id] + count < 4)
                            this.items[id] += count;
                        else
                            this.items[id] += free - 1;
                    } else {
                        if (count <= free)
                            this.items[id] = count;
                        else if (this.items[id] + count > 3)
                            this.items[id] = count - free;
                    }

                this.show();
            },
            count: function () {
                var count = 0;
                for (var id in this.items)
                    if (this.items.hasOwnProperty(id))
                        count += this.items[id];
                return count;
            },
            remove: function (id) {
                delete this.items[id];
                this.show();
            },
            getSum: function () {
                var sum = 0;
                for (var id in this.items)
                    if (this.items.hasOwnProperty(id))
                    //noinspection JSUnresolvedVariable
                        sum += Math.round(menu.itemList[id].amount2 / 4 * this.items[id]);

                return sum;
            },
            show: function () {
                var table = document.getElementById('table');
                var amount = document.getElementById('amount');

                table.innerHTML = '';

                for (var id in this.items)
                    if (this.items.hasOwnProperty(id))
                    //noinspection JSUnresolvedVariable
                        table.appendChild(generateElement({
                            tag: 'tr', itemId: id, elements: [
                                {tag: 'td', innerHtml: this.items[id] + '/4'},
                                {tag: 'td', innerHtml: menu.itemList[id].name},
                                {
                                    tag: 'td',
                                    innerHtml: Math.round(menu.itemList[id].amount2 / 4 * this.items[id]) + ' р.'
                                },
                                {
                                    tag: 'td', innerHtml: 'X', onclick: function () {
                                    if (menu.cart.pizza.items[this.parentNode.data.itemId] > 1)
                                        --menu.cart.pizza.items[this.parentNode.data.itemId];
                                    else
                                        delete menu.cart.pizza.items[this.parentNode.data.itemId];
                                    menu.cart.pizza.show();
                                }
                                }
                            ]
                        }));


                amount.innerHTML = 'Итого: ' + this.getSum() + ' р.';

                menu.cart.show();
            }
        },
        amount: document.getElementById('cart-amount'),
        sale: false,
        amountWithoutSale: 0,
        add: function (id, count) {
            if (id in this.items)
                this.items[id] += count;
            else
                this.items[id] = count;
            this.show();
        },
        remove: function (id, count) {
            if (count == 0)
                delete this.items[id];
            else
                this.items[id] = count;
            this.show();
        },
        show: function () {
            var sum = !!this.items['pizza'] ? this.pizza.getSum() : 0;
            for (var id in this.items)
                if (this.items.hasOwnProperty(id))
                    if (id != 'pizza')
                        sum += Math.round(menu.itemList[id].amount * this.items[id]);

            this.amountWithoutSale = sum;
            this.amount.innerHTML = sum + ' р';
        },
        getSaleAmount: function () {
            return Math.round(this.amountWithoutSale / 100 * config.sale);
        },
        getAmountWithSale: function () {
            return Math.round(this.amountWithoutSale - this.getSaleAmount());
        },
        send: function (data, cb) {
            l(data);
            var items = [];
            for (var id in this.items)
                if (this.items.hasOwnProperty(id))
                    if (id == 'pizza') {
                        var pizza = [];
                        for (var i in this.pizza.items)
                            if (this.pizza.items.hasOwnProperty(i))
                                pizza.push({id: menu.itemList[i].id, amount: this.pizza.items[i]});

                        items.push({id: id, data: pizza, amount: this.items[id]});
                    } else
                        items.push({id: menu.itemList[id].id, amount: this.items[id]});

            l(config.urls.buy, btoa(encodeURIComponent(JSON.stringify({items: items, data: data}))));

            getData(config.urls.buy,
                'POST',
                btoa(encodeURIComponent(JSON.stringify({items: items, data: data}))),
                function (r) {
                    if (!!cb)
                        cb(r);
                });
        }
    },
    user: {
        id: 0,
        phone: '+7',
        bdate: {month: 4, day: 2},
        name: '',
        latestOrder: 0,
        latestAddress: '',
        days: 0,
        isLogined: false,
        init: function (data) {
            if (!data)
                if (localStorage.hasOwnProperty('user'))
                    data = JSON.parse(localStorage.getItem('user'));
                else
                    return;
            this.id = data.id;
            this.phone = data.phone;

            if (!!window.Android)
                Android.saveUser(String(this.phone));

            this.name = data.name;
            this.latestOrder = data.latestOrder;
            this.latestAddress = data.latestAddress;

            if (!!this.phone)
                this.isLogined = true;

            var one = new Date(data.time * 1000);
            var two = new Date((data.latestOrder + 1209600) * 1000);
            var days = Math.round((two - one) / 1000 / 60 / 60 / 24);
            var s = one.addDays(days);
            if (days <= 14) {
                this.days = days;
                menu.cart.sale = true;
                document.getElementById('sale-top').innerHTML =
                    '<span>Скидка ' + config.sale + '%</span>';
            }
        },
        login: function (phone, cb) {
            if (!phone)
                phone = this.phone;
            if (!phone)
                return;
            if (String(phone).indexOf('+7') > -1)
                phone = String(phone).substr(2);

            getData(config.urls.login + phone, false, false, function (r) {
                localStorage.setItem('user', r);
                menu.isLogined = true;
                menu.user.init();
                if (!!cb)
                    cb();
            });
        },
        register: function (phone, cb) {
            if (!phone)
                return;
            if (phone.indexOf('+7') > -1)
                phone = phone.substr(2);

            getData(config.urls.register + phone + '&month=' + this.bdate.month + '&day=' + this.bdate.day, false, false, function (r) {
                localStorage.setItem('user', r);
                menu.isLogined = true;
                menu.user.init();
                if (!!cb)
                    cb();
            });
        }
    },
    history: ['main'],
    init: function () {
        this.generatePopup([{tag: div, innerHtml: 'Загрузка'}]);
        getData(config.urls.categories, false, false, function (r) {
            menu.menu = JSON.parse(r);
            menu.generateLeftMenu();

            getData(config.urls.items, false, false, function (r) {
                menu.itemList = JSON.parse(r);
                menu.user.init();

                if (menu.user.isLogined || location.hash.indexOf('debug')) {
                    menu.generateMainPage();
                } else {
                    menu.generateLoginPage(function () {
                        menu.generateMainPage();
                        /*if (menu.cart.sale)
                         menu.generateSalePage();*/
                    });
                }

                /*if (menu.cart.sale)
                 menu.generateSalePage();*/
            });
        });

        window.addEventListener('popstate', function (e) {
            menu.goto(location.hash.substr(1));
            e.preventDefault();
        }, false);
    },
    isOpen: function () {
        return this.top.classList.contains('right') && this.left.classList.contains('right');
    },
    open: function () {
        this.top.classList.remove('left');
        this.top.classList.add('right');
        this.left.classList.remove('left');
        this.left.classList.add('right');
    },
    close: function () {
        this.top.classList.remove('right');
        this.top.classList.add('left');
        this.left.classList.remove('right');
        this.left.classList.add('left');
    },
    show: function () {
        if (this.isOpen())
            this.close();
        else
            this.open();
    },
    goto: function (action, data) {
        window.scrollTo(0, 0);
        switch (action) {
            case 'main':
                this.generateMainPage();
                break;
            case 'item-list':
                this.generateItemListPage(data);
                break;
            case 'back':
                if (this.history.length == 0)
                    return;
                this.history.pop();
                this.goto(this.history.last());
                return;
            case 'sale':
                this.generateSalePage();
                break;
            case 'details':
                this.generateDetailsPage(data);
                break;
            case 'cart':
                this.generateCart();
                break;
            case 'checkout':
                this.generateCheckout();
                break;
            case 'login':
                this.generateLoginPage(data);
                break;
            case 'pizza':
                this.generatePizza();
                break;
            default :
                return;
        }
        if (this.history.last() != action) {
            this.history.push(action);
            location.hash = action;
        }
    },
    clearContent: function () {
        this.content.innerHTML = '';
        fixFixed.init();
    },
    appendContent: function (element) {
        this.content.appendChild(element);
        this.imageCheck();
        fixFixed.init();
    },
    clearLeftMenu: function () {
        this.left.innerHTML = '';
        fixFixed.init();
    },
    appendLeftMenu: function (element) {
        this.left.appendChild(element);
        fixFixed.init();
    },
    generateMainPage: function () {
        this.clearContent();
        this.appendContent(generateElement({
            tag: div, class: 'main', elements: [
                {
                    tag: img,
                    class: 'logo',
                    src: './img/logo.png'
                },
                {
                    tag: div,
                    class: 'buttons',
                    elements: [
                        {
                            tag: 'input', type: button, value: 'Меню', onclick: function () {
                            menu.goto('item-list');
                        }
                        },
                        {
                            tag: 'input', type: button, value: 'Ресторан', onclick: function () {
                            getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=restoran", 'get', null, function (r) {
                                var data = JSON.parse(r);
                                menu.generateCustomPage([
                                    {tag: div, class: 'title max-width', innerHtml: data.name},
                                    {tag: div, innerHtml: data.body}
                                ]);
                            });
                        }
                        },
                        {
                            tag: 'input', type: button, value: 'Баня', onclick: function () {
                            getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=banya", 'get', null, function (r) {
                                var data = JSON.parse(r);
                                menu.generateCustomPage([
                                    {tag: div, class: 'title max-width', innerHtml: data.name},
                                    {tag: div, innerHtml: data.body}
                                ]);
                            });
                        }
                        },
                        {
                            tag: 'input', type: button, value: 'Гостиница', onclick: function () {
                            getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=gostinitsa", 'get', null, function (r) {
                                var data = JSON.parse(r);
                                menu.generateCustomPage([
                                    {tag: div, class: 'title max-width', innerHtml: data.name},
                                    {tag: div, innerHtml: data.body}
                                ]);
                            });
                        }
                        }
                    ]
                },
                {
                    tag: 'span',
                    class: 'title',
                    innerHtml: 'Контакты'
                },
                {
                    tag: div,
                    class: 'contacts',
                    innerHtml: '<div><strong>Заказ банкета:</strong>(496)465-22-89</div>' +
                    '<div><strong>Доставка блюд:</strong>(496)467-25-25</div>' +
                    '<div><strong>Гостиница, Баня:</strong>(496)467-77-37</div>' +
                    '<div class="address">г. Раменское, ул. Королёва, д.27А</div>' +
                    '<div id="map"></div>'
                }
            ]
        }));
        initMap();
    },
    generateLeftMenu: function () {
        this.clearLeftMenu();

        this.appendLeftMenu(generateElement({
            tag: div,
            class: 'nav',
            elements: [
                {
                    tag: input,
                    class: 'red bg-white',
                    type: button,
                    value: 'Меню доставки',
                    onclick: function (e) {
                        if (e.target.nodeName != "INPUT") {
                            return;
                        }
                        var uls = document.getElementById('menu-left').getElementsByTagName('ul');
                        uls[0].classList.add('active');
                        uls[1].classList.remove('active');
                        document.getElementById('menu-left').getElementsByTagName('input')[1].className = 'white bg-red';
                        document.getElementById('menu-left').getElementsByTagName('input')[0].className = 'red bg-white';
                    }
                },
                {
                    tag: 'input',
                    class: 'white bg-red',
                    type: button,
                    value: 'Другие услуги',
                    onclick: function (e) {
                        if (e.target.nodeName != "INPUT") {
                            return;
                        }
                        var uls = document.getElementById('menu-left').getElementsByTagName('ul');
                        uls[1].classList.add('active');
                        uls[0].classList.remove('active');
                        document.getElementById('menu-left').getElementsByTagName('input')[0].className = 'white bg-red';
                        document.getElementById('menu-left').getElementsByTagName('input')[1].className = 'red bg-white';
                    }
                }
            ]
        }));

        var objectForGenerate = [];

        for (var i = 0; i < this.menu.length; i++) { //noinspection JSUnresolvedVariable
            if (this.menu[i].isTag) {
                objectForGenerate.push({
                    tag: 'li',
                    tagId: this.menu[i].id,
                    class: 'tag',
                    innerHtml: this.menu[i].name,
                    onclick: function () {
                        this.nextSibling.click();
                    }
                });
            } else {
                var indexOfTag = objectForGenerate.indexOf(getElementByParameter(objectForGenerate, this.menu[i].parent, 'tagId'));
                var newIndex = indexOfTag;
                for (var j = indexOfTag + 1; j < objectForGenerate.length; j++)
                    if (!objectForGenerate[j].tagId) {
                        if (objectForGenerate[j].order < this.menu[i].order)
                            newIndex++;
                    } else
                        break;
                var onclick;

                switch (this.menu[i].id) {
                    case 'sozday-svoyu-piccu':
                        onclick = function () {
                            menu.goto('pizza');
                        };
                        break;
                    default:
                        onclick = function () {
                            menu.goto('item-list', this.data);
                            menu.close();
                        };
                        break;
                }

                objectForGenerate.splice(newIndex + 1, 0, {
                    tag: 'li',
                    categoryId: this.menu[i].id,
                    order: this.menu[i].order,
                    innerHtml: this.menu[i].name,
                    onclick: onclick
                });
            }
        }

        this.appendLeftMenu(generateElement({
            tag: 'ul',
            class: 'active',
            elements: objectForGenerate
        }));

        this.appendLeftMenu(generateElement({
            tag: 'ul',
            elements: [
                {
                    tag: 'li', innerHtml: 'Меню', onclick: function () {
                    menu.goto('item-list');
                    menu.close();
                }
                },
                {
                    tag: 'li', innerHtml: 'Ресторан', onclick: function () {
                    getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=restoran", 'get', null, function (r) {
                        var data = JSON.parse(r);
                        menu.generateCustomPage([
                            {tag: div, class: 'title max-width', innerHtml: data.name},
                            {tag: div, innerHtml: data.body}
                        ]);
                    });
                    menu.close();
                }
                },
                {
                    tag: 'li', innerHtml: 'Баня', onclick: function () {
                    getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=banya", 'get', null, function (r) {
                        var data = JSON.parse(r);
                        menu.generateCustomPage([
                            {tag: div, class: 'title max-width', innerHtml: data.name},
                            {tag: div, innerHtml: data.body}
                        ]);
                    });
                    menu.close();
                }
                },
                {
                    tag: 'li', innerHtml: 'Гостиница', onclick: function () {
                    getData("http://l-polyana.info/app/data/php/getData.php?method=page&url=gostinitsa", 'get', null, function (r) {
                        var data = JSON.parse(r);
                        menu.generateCustomPage([
                            {tag: div, class: 'title max-width', innerHtml: data.name},
                            {tag: div, innerHtml: data.body}
                        ]);
                    });
                    menu.close();
                }
                }
            ]
        }));
        window.onresize = function () {
            initLeftMenu();
        };
        initLeftMenu();
    },
    generateItemListPage: function (data) {
        this.clearContent();

        var objectForGenerate = [];

        for (var i = 0; i < this.itemList.length; i++) {
            if (!getElementByParameter(objectForGenerate, this.itemList[i].parent, 'id')) {
                var parent = '';

                for (var j = 0; j < this.menu.length; j++)
                    if (this.menu[j].id == this.itemList[i].parent)
                        parent = this.menu[j].name;

                objectForGenerate.push({
                    tag: div,
                    id: this.itemList[i].parent,
                    class: 'category',
                    innerHtml: parent
                });
            }
            //noinspection JSUnresolvedVariable
            objectForGenerate.push({
                tag: div,
                class: 'item',
                itemId: i,
                elements: [
                    {tag: img, class: 'xz', src: this.itemList[i].src},
                    // {tag: img, class: 'xz'},
                    {tag: div, class: 'title', innerHtml: this.itemList[i].name},
                    {tag: button, class: 'amount white', innerHtml: this.itemList[i].amount + ' р'},
                    {tag: div, class: 'description', innerHtml: this.itemList[i].description},
                    {tag: div, class: 'weight black', innerHtml: this.itemList[i].weight}
                ],
                onclick: function () {
                    if (this.classList.contains('selected')) {
                        this.classList.remove('selected');
                        this.parentNode.removeChild(this.nextSibling);
                    } else {
                        var elements = this.parentNode.getElementsByClassName('item-selected');
                        for (var i = 0; i < elements.length; i++)
                            elements[i].parentNode.removeChild(elements[i]);

                        elements = this.parentNode.getElementsByClassName('selected');
                        for (i = 0; i < elements.length; i++)
                            elements[i].classList.remove('selected');

                        this.classList.add('selected');
                        this.parentNode.insertAfter(generateElement({
                            tag: div, class: 'item-selected',
                            elements: [
                                {
                                    tag: button, class: 'to-cart white', innerHtml: 'В корзину',
                                    onclick: function () {
                                        //noinspection JSPotentiallyInvalidUsageOfThis
                                        var data = this.parentNode.previousSibling.data;
                                        if (data.itemId == undefined)
                                            return;
                                        menu.generatePreAddCart(data);
                                    }
                                },
                                {
                                    tag: button, class: 'to-details white', innerHtml: 'Детали',
                                    onclick: function () {
                                        //noinspection JSPotentiallyInvalidUsageOfThis
                                        var data = this.parentNode.previousSibling.data;
                                        if (data.itemId == undefined)
                                            return;
                                        menu.goto('details', {itemId: data.itemId});
                                    }
                                }
                            ]
                        }), this);
                    }
                }
            });
        }

        this.appendContent(generateElement({
            tag: div,
            class: 'items',
            elements: objectForGenerate
        }));

        setTimeout(function () {
            if (!!data && !!data.categoryId) {
                menu.content.getElementsByClassName('category')[data.categoryId].scrollIntoView();
                window.scrollBy(0, -60);
            }
        }, 5);
    },
    generateSalePage: function () {
        this.appendContent(generateElement({
            tag: div, class: 'sale-main', elements: [
                {
                    tag: 'span', class: 'close white', innerHtml: 'X', onclick: function () {
                    this.parentNode.parentNode.removeChild(this.parentNode);
                }
                },
                {tag: div, class: 'amount yellow', innerHtml: config.sale + '%'},
                {
                    tag: 'p',
                    class: 'date yellow',
                    innerHtml: 'Ваша скидка закончится через ' + this.user.days + ' день, поторопитесь!'
                },
                {
                    tag: 'p',
                    class: 'description white',
                    innerHtml: 'Сделав заказ на доставку любых блюд в течении 14 дней вы получите скидку ' + config.sale + '%'
                },
                {
                    tag: 'a', class: 'yellow', innerHtml: 'Условия акции',
                    onclick: function () {
                        menu.generateCustomPage([
                            {tag: p, class: 'title', innerHtml: 'Условия акции'}, {
                                tag: p, innerHtml: config.sale + '% скидка на доставку!<br>' +
                                'Дорогие друзья, принимайте участие в акции "-' + config.sale + '% на доставку", получайте и удерживайте скидку!' +
                                '<ol>' +
                                '<li>В акции принимают участие заказы, сделанные с помощью мобильного приложения</li>' +
                                '<li>Сделав, первый заказ Вы получите скидку ' + config.sale + '%</li>' +
                                '<li>Удерживайте скидку ' + config.sale + '%, делая повторные заказы не позднее 14 дней от каждого предыдущего. Отчет начинается со дня оформления заказа.</li>' +
                                '<li>акция распространяется в том числе на услугу "Самовывоз".</li>' +
                                '<li>Скидки по акциям не суммируются.</li>' +
                                '</ol>'
                            }]);
                    }
                }
            ]
        }));
    },
    generateDetailsPage: function (data) {
        if (!data)
            return;
        this.clearContent();
        var itemId = data.itemId;
        //noinspection JSUnresolvedVariable
        this.appendContent(generateElement({
            tag: div,
            class: 'item-big',
            elements: [
                {tag: img, src: this.itemList[itemId].srcBig},
                {
                    tag: div, class: 'about',
                    elements: [
                        {tag: 'span', class: 'title', innerHtml: this.itemList[itemId].name},
                        {tag: 'span', class: 'description', innerHtml: this.itemList[itemId].description},
                        {tag: 'span', class: 'weight', innerHtml: this.itemList[itemId].weight},
                        {
                            tag: div, class: 'xz',
                            elements: [
                                {
                                    tag: button, class: 'back bg-red white', innerHtml: 'Назад',
                                    onclick: function () {
                                        history.back();
                                    }
                                },
                                {tag: 'span', class: 'amount red', innerHtml: this.itemList[itemId].amount + ' р.'},
                                {
                                    tag: button, class: 'to-cart white', innerHtml: 'В корзину',
                                    onclick: function () {
                                        if (data.itemId == undefined)
                                            return;
                                        menu.generatePreAddCart(data);
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }));
    },
    generatePreAddCart: function (data) {
        var lis = [];
        for (var i = 1; i < 21; i++)
            lis.push({
                tag: 'li', innerHtml: String(i), onclick: function () {
                    menu.cart.add(data.itemId, Number(this.innerHTML));
                    menu.content.removeChild(this.parentNode.parentNode.parentNode);
                }
            });
        this.generatePopup([{tag: div, innerHtml: 'Количество порций'},
            {tag: 'ul', class: 'range', elements: lis}]);
    },
    generateCart: function () {
        if (this.cart.items.size() == 0) {
            this.goto('back');
            return;
        }

        var items = [];
        var left = [];
        var right = [];

        var checkout = function () {
            menu.goto('checkout');
        };
        if (this.cart.sale) {
            left = [
                {tag: 'span', class: 'amount-old', innerHtml: menu.cart.amountWithoutSale},
                {tag: br},
                {
                    tag: 'span', class: 'amount-new', elements: [
                    {tag: 'span', innerHtml: 'Скидка'},
                    {tag: 'span', class: 'sale', innerHtml: config.sale + '%'},
                    {tag: 'span', class: 'sale-amount', innerHtml: menu.cart.getSaleAmount()}
                ]
                }
            ];
            right = [
                {tag: 'span', class: 'amount red', innerHtml: menu.cart.getAmountWithSale()},
                {tag: br},
                {tag: 'span', class: 'issue red', innerHtml: 'Оформить', onclick: checkout}
            ];
        } else
            right = [
                {tag: 'span', class: 'amount red', innerHtml: menu.cart.amountWithoutSale},
                {tag: br},
                {tag: 'span', class: 'issue red', innerHtml: 'Оформить', onclick: checkout}];
        items.push({
            tag: div, class: 'sale-top', elements: [
                {tag: img, class: 'left', src: './img/Jtsuytsu.png'},
                {tag: div, class: 'left', elements: left},
                {tag: div, class: 'right', elements: right}
            ]
        });

        for (var id in menu.cart.items)
            if (menu.cart.items.hasOwnProperty(id))
                if (id == 'pizza')
                    items.push({
                        tag: div, itemId: id, class: 'cart-item item', elements: [
                            {tag: img, src: 'img/pizza.png'},
                            {tag: div, class: 'title', innerHtml: 'Собери свою пиццу'},
                            {
                                tag: button,
                                class: 'amount white',
                                innerHtml: menu.cart.items[id] + ' x' + menu.cart.pizza.getSum() + ' р',
                                onclick: function () {
                                    var itemId = this.parentNode.data.itemId;

                                    var lis = [];
                                    for (var i = 0; i < 21; i++)
                                        lis.push({
                                            tag: 'li', innerHtml: String(i), onclick: function () {
                                                menu.cart.remove(itemId, Number(this.innerHTML));
                                                menu.generateCart();
                                            }
                                        });
                                    menu.generatePopup([
                                        {tag: div, innerHtml: 'Количество порций'},
                                        {tag: 'ul', class: 'range', elements: lis}
                                    ]);
                                }
                            }
                        ]
                    });
                else
                //noinspection JSUnresolvedVariable
                    items.push({
                        tag: div, itemId: id, class: 'cart-item item', elements: [
                            {tag: img, src: menu.itemList[id].srcMin},
                            {tag: div, class: 'title', innerHtml: menu.itemList[id].name},
                            {
                                tag: button,
                                class: 'amount white',
                                innerHtml: menu.cart.items[id] + ' x' + menu.itemList[id].amount + ' р',
                                onclick: function () {
                                    var itemId = this.parentNode.data.itemId;

                                    var lis = [];
                                    for (var i = 0; i < 21; i++)
                                        lis.push({
                                            tag: 'li', innerHtml: String(i), onclick: function () {
                                                menu.cart.remove(itemId, Number(this.innerHTML));
                                                menu.generateCart();
                                            }
                                        });
                                    menu.generatePopup([
                                        {tag: div, innerHtml: 'Количество порций'},
                                        {tag: 'ul', class: 'range', elements: lis}
                                    ]);
                                }
                            }
                        ],
                        onclick: function (e) {
                            if (e.target.nodeName != "BUTTON")
                                menu.goto('details', this.data);
                        }
                    });

        this.clearContent();
        this.appendContent(generateElement({tag: div, class: 'cart-items items', elements: items}));
    },
    generateCheckout: function () {
        this.clearContent();
        this.appendContent(generateElement({
            tag: div, class: 'checkout', elements: [
                {tag: div, class: 'title', innerHtml: 'Оформление заказа'},
                {
                    tag: 'input',
                    id: 'phone',
                    type: 'text',
                    placeholder: 'Телефон',
                    value: '+7' + this.user.phone || '+7',
                    oninput: validatePhone
                },
                {tag: 'input', id: 'name', type: 'text', placeholder: 'Имя', value: this.user.name},
                {tag: 'textarea', id: 'address', placeholder: 'Адрес доставки', value: this.user.latestAddress},
                {tag: 'textarea', id: 'comment', placeholder: 'Коментарий'},
                {
                    tag: 'input',
                    type: 'submit',
                    class: 'bg-red white max-width',
                    value: 'Оформить заказ',
                    onclick: function () {
                        if (!menu.user.isLogined) {
                            menu.user.phone = document.getElementById('phone').value;
                            menu.goto('login');
                        } else {
                            menu.generatePopup([{tag: div, innerHtml: 'Отправка запроса'}]);
                            menu.cart.send({
                                phone: document.getElementById('phone').value.substr(2),
                                name: document.getElementById('name').value,
                                address: document.getElementById('address').value,
                                comment: document.getElementById('comment').value
                            }, function (r) {
                                menu.generatePopup([
                                    {tag: div, innerHtml: r[0] ? 'Покупка совершена' : 'Ошибка отправки данных'},
                                    {
                                        tag: button,
                                        class: 'max-width bg-red white',
                                        innerHtml: 'Закрыть',
                                        onclick: function () {
                                            menu.user.login();
                                            menu.goto('back');
                                        }
                                    }
                                ])
                            });
                        }
                    }
                },
                {
                    tag: div,
                    class: 'title left max-width text-start inline-block',
                    innerHtml: 'График работы доставки:'
                },
                {
                    tag: 'p',
                    class: 'left max-width text-start inline-block',
                    innerHtml: 'Вс - Чт с 11:00 до 23:00<br>Пт - Сб с 11:00 до 5:00'
                },
                {tag: div, class: 'title left max-width text-start inline-block', innerHtml: 'Бесплатная доставка:'},
                {
                    tag: 'p',
                    class: 'left max-width text-start inline-block',
                    innerHtml: '+7 (496) 46 7-25-25<br> +7 (496) 46 3-09-03'
                }
            ]
        }))
    },
    generateLoginPage: function (cb) {
        this.clearContent();
        var days = [{tag: 'option', selected: 'selected', value: 0, disabled: 'disabled', innerHtml: 'День'}];
        for (var i = 0; i < 31; i++)
            days.push({tag: 'option', value: i + 1, innerHtml: i + 1});

        var months = [
            {tag: 'option', selected: 'selected', value: 0, disabled: 'disabled', innerHtml: 'Месяц'},
            {tag: 'option', value: 1, innerHtml: 'Январь'},
            {tag: 'option', value: 2, innerHtml: 'Февраль'},
            {tag: 'option', value: 3, innerHtml: 'Март'},
            {tag: 'option', value: 4, innerHtml: 'Апрель'},
            {tag: 'option', value: 5, innerHtml: 'Май'},
            {tag: 'option', value: 6, innerHtml: 'Июнь'},
            {tag: 'option', value: 7, innerHtml: 'Июль'},
            {tag: 'option', value: 8, innerHtml: 'Август'},
            {tag: 'option', value: 9, innerHtml: 'Сентябрь'},
            {tag: 'option', value: 10, innerHtml: 'Октябрь'},
            {tag: 'option', value: 11, innerHtml: 'Ноябрь'},
            {tag: 'option', value: 12, innerHtml: 'Декабрь'}
        ];

        this.appendContent(generateElement({
            tag: div, class: 'login', elements: [
                {tag: div, class: 'title', innerHtml: 'Вход'},
                {tag: 'input', type: 'text', placeholder: 'Телефон', oninput: validatePhone, value: this.user.phone},
                {tag: div, class: 'title', innerHtml: 'Дата рождения'},
                {
                    tag: 'select', elements: months, onchange: function () {
                    switch (this.value) {
                        case '1'://я
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '2'://ф
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 30)
                            }), this);
                            break;
                        case '3'://м
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '4'://а
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 31)
                            }), this);
                            break;
                        case '5'://м
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '6'://и
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 31)
                            }), this);
                            break;
                        case '7'://и
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '8'://а
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '9'://с
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 31)
                            }), this);
                            break;
                        case '10'://о
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                        case '11'://н
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 31)
                            }), this);
                            break;
                        case '12'://д
                            this.parentNode.removeChild(this.nextElementSibling);
                            this.parentNode.insertAfter(generateElement({
                                tag: 'select',
                                elements: days.slice(0, 32)
                            }), this);
                            break;
                    }
                }
                },
                {tag: 'select', elements: days},
                {
                    tag: 'input', class: 'bg-red white', type: button, value: 'Вход', onclick: function () {
                    menu.user.login(this.parentNode.getElementsByTagName('input')[0].value);
                    menu.generatePopup([
                        {tag: div, class: 'max-width', innerHtml: 'Вы успешно вошли'},
                        {
                            tag: button,
                            class: 'max-width bg-red white',
                            innerHtml: 'Закрыть',
                            onclick: function () {
                                if (!!cb)
                                    return cb();
                                menu.goto('back');
                            }
                        }
                    ]);
                }
                },
                {
                    tag: 'input', class: 'bg-red white', type: button, value: 'Регистрация', onclick: function () {
                    menu.user.register(this.parentNode.getElementsByTagName('input')[0].value, function () {
                        menu.generatePopup([
                            {tag: div, class: 'max-width', innerHtml: 'Регистрация прошла успешно'},
                            {
                                tag: button,
                                class: 'max-width bg-red white',
                                innerHtml: 'Закрыть',
                                onclick: function () {
                                    if (!!cb)
                                        return cb();
                                    menu.goto('back');
                                }
                            }
                        ]);
                    });
                }
                }
            ]
        }));
    },
    generatePopup: function (elements, onclick) {
        this.appendContent(generateElement({
            tag: div, class: 'popup fixed-supported', elements: [
                {
                    tag: div, class: 'bg', onclick: !!onclick ? onclick : function () {
                    menu.content.removeChild(this.parentNode);
                }
                },
                {tag: div, class: 'c', elements: elements}
            ]
        }));
    },
    generateCustomPage: function (elements) {
        this.clearContent();
        this.appendContent(generateElement({tag: div, class: 'custom-page', elements: elements}));
    },
    imageCheck: function () {
        var images = this.content.getElementsByTagName(img);
        for (var i = 0; i < images.length; i++)
            images[i].onerror = function () {
                if (this.width == 85)
                    this.src = config.images.small;
                if (this.width == 125)
                    this.src = config.images.normal;
                if (this.width == 0)
                    this.src = config.images.large;
            }
    },
    generatePizza: function () {
        var elements = [];

        elements.push({
            tag: div, class: 'top', elements: [
                {tag: div, class: 'title max-width bold', innerHtml: 'Собери свою пиццу'},
                {tag: img, class: 'left', src: 'img/pizza.png'},
                {
                    tag: div, class: 'right red table', elements: [
                    {tag: 'table', id: 'table'},
                    {
                        tag: button, class: 'bg-red white', innerHtml: 'Заказать',
                        onclick: function () {
                            if (menu.cart.pizza.count() == 4) {
                                menu.cart.add('pizza', 1);
                                menu.generatePopup([
                                    {tag: div, innerHtml: 'Пицца добавлена в корзину'},
                                    {
                                        tag: button,
                                        class: 'max-width bg-red white',
                                        innerHtml: 'Закрыть',
                                        onclick: function () {
                                            menu.content.removeChild(this.parentNode.parentNode);
                                        }
                                    }
                                ]);
                            } else
                                menu.generatePopup([
                                    {tag: div, innerHtml: 'Пицца должна состоять из 4-х кусков'},
                                    {
                                        tag: button,
                                        class: 'max-width bg-red white',
                                        innerHtml: 'Закрыть',
                                        onclick: function () {
                                            menu.content.removeChild(this.parentNode.parentNode);
                                        }
                                    }]);
                        }
                    },
                    {tag: span, id: 'amount', class: 'red right bold', innerHtml: 'Итого: 850 р.'}
                ]
                }
            ],
            onclick: function () {
                menu.goto('pizza');
            }
        });

        for (var i = 0; i < this.itemList.length; i++)
            //noinspection JSValidateTypes,JSUnresolvedVariable
            if (this.itemList[i].parent == 'picci' && !!this.itemList[i].amount2)
            //noinspection JSUnresolvedVariable
                elements.push({
                    tag: div, class: 'item', itemId: i, elements: [
                        {tag: img, src: this.itemList[i].src},
                        {tag: div, class: 'title', innerHtml: this.itemList[i].name},
                        {
                            tag: div, class: 'buttons right', elements: [
                            {
                                tag: button,
                                class: 'amount white',
                                innerHtml: '1/2 ' + Math.round(this.itemList[i].amount2 / 2) + ' р'
                            },
                            {tag: br},
                            {
                                tag: button,
                                class: 'amount white',
                                innerHtml: '1/4 ' + Math.round(this.itemList[i].amount2 / 4) + ' р'
                            }
                        ]
                        },
                        {tag: div, class: 'description', innerHtml: this.itemList[i].description}
                    ],
                    onclick: function () {
                        if (this.classList.contains('selected')) {
                            this.classList.remove('selected');
                            this.parentNode.removeChild(this.nextSibling);
                        } else {
                            var elements = this.parentNode.getElementsByClassName('item-selected');
                            for (var i = 0; i < elements.length; i++)
                                elements[i].parentNode.removeChild(elements[i]);

                            elements = this.parentNode.getElementsByClassName('selected');
                            for (i = 0; i < elements.length; i++)
                                elements[i].classList.remove('selected');

                            this.classList.add('selected');
                            this.parentNode.insertAfter(generateElement({
                                tag: div, class: 'item-selected',
                                elements: [
                                    {
                                        tag: button, class: 'to-cart white', innerHtml: '1/2',
                                        onclick: function () {
                                            //noinspection JSPotentiallyInvalidUsageOfThis
                                            var data = this.parentNode.previousSibling.data;
                                            if (data.itemId == undefined)
                                                return;
                                            menu.cart.pizza.add(data.itemId, 2);
                                        }
                                    },
                                    {
                                        tag: button, class: 'to-cart white', innerHtml: '1/4',
                                        onclick: function () {
                                            //noinspection JSPotentiallyInvalidUsageOfThis
                                            var data = this.parentNode.previousSibling.data;
                                            if (data.itemId == undefined)
                                                return;
                                            menu.cart.pizza.add(data.itemId, 1);
                                        }
                                    },
                                    {
                                        tag: button, class: 'to-details white', innerHtml: 'Детали',
                                        onclick: function () {
                                            //noinspection JSPotentiallyInvalidUsageOfThis
                                            var data = this.parentNode.previousSibling.data;
                                            if (data.itemId == undefined)
                                                return;
                                            menu.goto('details', {itemId: data.itemId});
                                        }
                                    }
                                ]
                            }), this);
                        }
                    }
                });

        this.clearContent();
        this.appendContent(generateElement({tag: div, class: 'pizza items', elements: elements}));
        this.cart.pizza.show();
    }
};

//noinspection SpellCheckingInspection
window.onload = function () {
    menu.init();
};