function sendForm(form, url, callBack) {
    if (!document.createElement)
        return; // not supported
    if (typeof(form) == "string")
        form = document.getElementById(form);
    var iFrame = createIFrame(Math.random());
    iFrame.addEventListener('load', function () {
        callBack(getIFrameXML(iFrame).getElementsByTagName('body')[0].innerHTML);
        document.body.removeChild(iFrame);
    });
    form.setAttribute('target', iFrame.id);
    form.setAttribute('action', url);
    form.submit();
}

function getIFrameXML(iFrame) {
    var doc = iFrame.contentDocument;
    if (!doc && iFrame.contentWindow)
        doc = iFrame.contentWindow.document;
    if (!doc)
        doc = window.frames[iFrame.id].document;
    if (!doc)
        return null;
    if (doc.location == "about:blank")
        return null;
    if (doc.XMLDocument)
        doc = doc.XMLDocument;
    return doc;
}

function createIFrame(name, src, debug) {
    src = src || 'javascript:false'; // пустой src

    var tmpElem = document.createElement('div');

    // в старых IE нельзя присвоить name после создания iframe
    // поэтому создаём через innerHTML
    tmpElem.innerHTML = '<iframe name="' + name + '" id="' + name + '" src="' + src + '">';
    var iFrame = tmpElem.firstChild;

    if (!debug)
        iFrame.style.display = 'none';

    document.body.appendChild(iFrame);

    return iFrame;
}