function registerSwipe(element, left, right, top, bottom) {
    element.addEventListener("touchstart", function (evt) {
        //evt.preventDefault();
        startX = evt.changedTouches[0].clientX;
        startY = evt.changedTouches[0].clientY;
    }, false);
    element.addEventListener("touchend", function (evt) {
        //evt.preventDefault();
        if (startX < 40 && startX < evt.changedTouches[0].clientX - 30 && !!right)
            right(evt);

        if (startX > evt.changedTouches[0].clientX - 30 && !!left)
            left(evt);

        if (startY > evt.changedTouches[0].clientY - 30 && !!top)
            top(evt);

        if (startY < evt.changedTouches[0].clientY - 30 && !!bottom)
            bottom(evt);
    }, false);

    var startX = 0;
    var startY = 0;
}

registerSwipe(
    document.body,
    function () {
        if (menu.isOpen())
            menu.close();
    },
    function () {
        if (!menu.isOpen())
            menu.show();
    },
    false,
    function () {
        //location.reload();
    }
);