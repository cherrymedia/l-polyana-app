function initLeftMenu() {
    var xz = document.getElementsByClassName('menu-left');
    for (var i = 0; i < xz.length; i++) {
        var uls = xz[i].getElementsByTagName('ul');
        for (var j = 0; j < uls.length; j++)
            uls[j].style.height = screen.height - 80 + 'px';
    }
}

function initMap() {
    ymaps.ready(init);
    function init() {
        var myMap = new ymaps.Map('map', {
            center: [55.589091, 38.203795],
            zoom: 17
        });
        myMap.disableDragging();
        //myMap.behaviors.disable('drag');
        myMap.geoObjects.add(new ymaps.Placemark([55.589091, 38.203795]));
    }
}

function isIOS() {
    return /iPad|iPhone|iPod/.test(navigator.platform);
}

var fixFixed = {
    elements: [],
    width: 0,
    height: 0,
    init: function () {
        this.width = document.documentElement;
        this.height = document.documentElement;
        this.elements = document.getElementsByClassName('fixed-supported');
        var scrolledX = window.pageXOffset || document.documentElement.scrollLeft;
        var scrolledY = window.pageYOffset || document.documentElement.scrollTop;
        for (var i = 0; i < this.elements.length; i++) {
            this.elements[i].style.position = 'absolute';
            var coordinates = fixFixed.elements[i].getBoundingClientRect();
            this.elements[i].fixFixed = {
                top: scrolledY + parseInt(coordinates.top),
                left: scrolledX + parseInt(coordinates.left)
            };
        }
        window.onscroll = this.scroll;
        this.scroll();
        return this;
    },
    scroll: function () {
        var scrolledX = window.pageXOffset || document.documentElement.scrollLeft;
        var scrolledY = window.pageYOffset || document.documentElement.scrollTop;
        for (var i = 0; i < fixFixed.elements.length; i++) {
            fixFixed.elements[i].style.top = scrolledY + fixFixed.elements[i].fixFixed.top + 'px';
            fixFixed.elements[i].style.left = scrolledX + fixFixed.elements[i].fixFixed.left + 'px';
        }
    }
};

function getElementByParameter(array, parameter, parameterName) {
    for (var i in array)
        if (array.hasOwnProperty(i) && array[i][parameterName] == parameter)
            return array[i];
    return false;
}

var xzargs = {
    tag: 'input',
    type: 'input',
    innerHtml: 'innerHtml',
    name: '',
    action: '',
    method: '',
    value: '',
    class: '',
    disabled: false,
    selected: false,
    placeholder: '',
    style: '',
    src: '',
    href: '#',
    onclick: function () {
    },
    onload: function () {
    },
    oninput: function () {
    },
    onchange: function () {
    },
    id: '',
    elements: []
};
function generateElement(args, toAppend) {
    var element = !!toAppend ? toAppend : document.createElement(args.tag);
    if (!!args.id)
        element.id = args.id;
    if (!!args.type)
        element.type = args.type;
    if (!!args.class)
        element.className = args.class;
    if (!!args.disabled)
        element.disabled = args.disabled;
    if (!!args.selected)
        element.selected = args.selected;
    if (!!args.placeholder)
        element.placeholder = args.placeholder;
    if (!!args.style)
        element.style = args.style;
    if (!!args.src)
        element.src = args.src;
    if (!!args.name)
        element.name = args.name;
    if (!!args.action)
        element.action = args.action;
    if (!!args.method)
        element.method = args.method;
    if (!!args.value)
        element.value = args.value;
    if (!!args.onclick) {
        if (isIOS())
            element.addEventListener('touchend', args.onclick, false);
        else
            element.onclick = args.onclick;
    }
    if (!!args.onload)
        element.onload = args.onload;
    if (!!args.oninput)
        element.oninput = args.oninput;
    if (!!args.onchange)
        element.onchange = args.onchange;
    if (!!args.innerHtml)
        element.innerHTML = args.innerHtml;
    if (!!args.elements && args.elements.length > 0)
        for (var i = 0; i < args.elements.length; i++)
            if (args.elements[i] instanceof Node)
                element.appendChild(args.elements[i]);
            else
                element.appendChild(generateElement(args.elements[i]));

    args.element = element;
    element.data = args;

    return element;
}
Node.prototype.insertAfter = function (elem, refElem) {
    return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
};

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function validatePhone() {
    var chars = this.value.split('');
    this.value = '';
    for (var i = 0; i < chars.length; i++)
        if (isNumber(chars[i]))
            if (!(((i == 0 || i == 1) && chars[i] == 7) || ((i == 0 || i == 1) && chars[i] == 8)))
                this.value += chars[i];

    if (this.value.length > 10)
        this.value = this.value.substr(0, 10);

    this.value = '+7' + this.value;
}

function getItemById(array, id) {
    for (var i in array)
        if (array.hasOwnProperty(i) && array[i].id == id)
            return array[i];
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

var XMLHttpSupport = true;

function getData(url, method, data, cb) {
    if (!method) {
        method = 'get';
    }
    if (!data) {
        data = '';
    }

    if (XMLHttpSupport) {
        try {
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;

            xhr.open(method, url);

            if (!!data) {
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send('data=' + encodeURIComponent(data));
            } else
                xhr.send();
        } catch (e) {
            XMLHttpSupport = false;
            getData(url, method, data, cb);
        }

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4)
                if (this.status >= 200 && this.status < 400 && !!cb)
                    cb(xhr.responseText);
                else
                    l(xhr.status + ': ' + xhr.statusText);
        };
    } else {
        url = url + ((url.indexOf('?') > 0) ? '&' : '?') + 'script=sD&data=' + encodeURIComponent(data);
        var script = generateElement({
            tag: 'script', src: url, onload: function () {
                //noinspection JSUnresolvedVariable
                l(sD);
                if (!!cb)
                //noinspection JSUnresolvedVariable
                    cb(JSON.stringify(sD));
                //noinspection JSCheckFunctionSignatures
                document.body.removeChild(this);
            }
        });
        document.body.appendChild(script);
    }
}

function l() {
    for (var i = 0; i < arguments.length; i++)
        console.log(arguments[i]);
}

Array.prototype.last = function () {
    return this[this.length - 1];
};
Array.prototype.remove = function (index) {
    return this.splice(index, 1);
};

Object.prototype.size = function () {
    var size = 0;
    for (var key in this)
        if (this.hasOwnProperty(key))
            size++;
    return size;
};

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

//= test.js