<?php

/**
 * Created by PhpStorm.
 * User: RedCreepster
 * Date: 24.06.15
 * Time: 14:03
 */
class ImageReducer {
    /**
     * @var string
     */
    private $srcUrl;
    /**
     * @var int
     */
    private $width;
    /**
     * @var int
     */
    private $height;
    /**
     * @var bool
     */
    private $cache;

    /**
     * @var resource
     */
    private $resizeImg = false;

    public function __construct($srcUrl, $width, $height, $cache = true) {
        $this->height = $height;
        $this->srcUrl = $srcUrl;
        $this->width = $width;
        $this->cache = $cache;
    }

    /**
     * @return bool
     */
    public function isCached() {
        return file_exists($this->getLocalPath() . '.jpg');
    }

    private function restoreFromCache() {
        $this->resizeImg = imagecreatefromjpeg($this->getLocalPath() . '.jpg');
    }

    private function resize() {
        $src = imagecreatefromjpeg($this->srcUrl);
        list($w, $h) = getimagesize($this->srcUrl);

        $this->resizeImg = imagecreatetruecolor($this->width, $this->height);
        imageinterlace($this->resizeImg, true);
        imagecopyresized($this->resizeImg, $src, 0, 0, 0, 0, $this->width, $this->height, $w, $h);

        imagedestroy($src);
    }

    private function saveInToCache() {
        imagejpeg($this->resizeImg, $this->getLocalPath() . '.jpg');
    }

    public function getLocalPath() {
        return Config::getDirToCache() . base64_encode('src=' . $this->srcUrl . 'w=' . $this->width . 'h=' . $this->height . 'length=' . curl_get_file_size($this->srcUrl));
    }

    public function getResizeUrl() {
        if ($this->isCached()) {
            return Config::getUrlToCacheDir() . base64_encode('src=' . $this->srcUrl . 'w=' . $this->width . 'h=' . $this->height . 'length=' . curl_get_file_size($this->srcUrl)) . '.jpg';
        } else {
            return Config::getUrlToReducer() . '?method=resizeImage&url=' . $this->srcUrl . '&w=' . $this->width . '&h=' . $this->height;
        }
    }

    public function showImage() {
        if (!$this->resizeImg) {
            if ($this->isCached()) {
                $this->restoreFromCache();
            } else {
                $this->resize();
                if ($this->cache) {
                    $this->saveInToCache();
                }
            }
        }
        header('Content-Type: image/jpeg');
        imagejpeg($this->resizeImg);
    }

    public function cache() {
        if (!$this->isCached()) {
            $this->resize();
            if ($this->cache) {
                $this->saveInToCache();
            }
        }
    }

    public function close() {
        imagedestroy($this->resizeImg);
    }
}