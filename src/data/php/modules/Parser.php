<?php

/**
 * Created by PhpStorm.
 * User: redcreepster
 * Date: 23.06.15
 * Time: 12:16
 */
class Parser
{

    private $data = array();

    function __construct($data)
    {
    }

    function getById($id, $data = false)
    {
        if (!$data)
            $data = $this->data;
        foreach ($data as $key => $value) {
            if ($value->_id == $id)
                return $value;
            if (is_array($value) && $this->getById($id, $value))
                return $this->getById($id, $value);
        }
        return false;

    }
}