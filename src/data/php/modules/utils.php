<?php

function echoJson($data, $pre = '', $post = '', $force_object = false) {
    $json = $force_object ? json_encode($data, JSON_FORCE_OBJECT | JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE) : json_encode($data, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
    header('Content-Type: application/json');
    echo $pre . $json . $post;
}

function array_index_of($array, $elementOrKey) {
    $i = 0;
    foreach ($array as $key => $value) {
        if (
            $key == $elementOrKey || $value == $elementOrKey ||
            (isset($value['id']) && $value['id'] == $elementOrKey) ||
            (isset($value['_id']) && $value['_id'] == $elementOrKey)
        )
            return $i;
        $i++;
    }
    return false;
}

function curl_get_file_size($url) {
    $fp = curl_init();
    curl_setopt($fp, CURLOPT_NOBODY, true);
    curl_setopt($fp, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($fp, CURLOPT_FAILONERROR, 1);
    curl_setopt($fp, CURLOPT_REFERER, '');
    curl_setopt($fp, CURLOPT_URL, $url);
    curl_setopt($fp, CURLOPT_HEADER, 1);
    curl_setopt($fp, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_exec($fp);

    $result = curl_getinfo($fp, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

    return $result;
}

function getPostGetCookie($key) {
    $value = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRIPPED);
    if (!$value)
        $value = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRIPPED);
    if (!$value)
        $value = filter_input(INPUT_COOKIE, $key, FILTER_SANITIZE_STRIPPED);

    return $value;
}

function now() {
    $date = new DateTime();
    return $date->getTimestamp();
}