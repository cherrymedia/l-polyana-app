<?php

/**
 * Created by PhpStorm.
 * User: RedCreepster
 * Date: 23.06.15
 * Time: 10:47
 */
class db {

    /**
     * @var db
     */
    private static $db = false;

    /**
     * @var PDO
     */
    private $DBH = false;

    /**
     * @var PDOStatement
     */
    private $STH = false;

    private function __construct() {
        try {
            $this->DBH = new PDO("mysql:host=" . Config::getHost() . ";dbname=" . Config::getDb() . ";charset=UTF8", Config::getUser(), Config::getPassword());
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
            file_put_contents('PDOErrors.txt', $e->getMessage() . "\n\r", FILE_APPEND);
        }
    }

    /**
     * @param $sql string
     * @param $data array
     * @return $this db
     */
    public function query($sql, $data = array()) {
        $this->STH = $this->DBH->prepare($sql);
        $this->STH->execute($data);
        return $this;
    }

    /**
     * @return PDOStatement
     */
    public function getWithObject() {
        $this->STH->setFetchMode(PDO::FETCH_OBJ);
        return $this->STH;
    }

    public function lastInsertId() {
        return $this->DBH->lastInsertId();
    }

    /**
     *
     */
    public function close() {
        $this->DBH = null;
    }

    /**
     * @return db
     */
    public static function getInstance() {
        if (!self::$db) {
            self::$db = new db();
        }
        return self::$db;
    }
}