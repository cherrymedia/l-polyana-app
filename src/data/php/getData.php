<?php

error_reporting(E_ERROR);

include_once('Config.php');
include_once('modules/db.php');
include_once('modules/utils.php');
include_once('modules/ImageReducer.php');
include_once('modules/crossDomainHeaders.php');

$method = getPostGetCookie('method');

getData($method);

function getData($method) {
    switch ($method) {
        case 'items':
            $db = db::getInstance();
            $objects = $db->query("SELECT p.id, p.url, p.name, p.annotation, (SELECT filename FROM s_images WHERE product_id = p.id AND NOT filename IS NULL LIMIT 1) AS filename, (SELECT variant_name FROM s_purchases WHERE p.id = s_purchases.product_id LIMIT 1) AS variant_name, (SELECT price FROM s_purchases WHERE p.id = s_purchases.product_id LIMIT 1) AS price FROM s_products p WHERE p.visible != 0 ORDER BY p.position;")->getWithObject();
//            $data = $items;
            $buf = [];
            $data = [];
            while ($object = $objects->fetch()) {
                if (empty($object->price)) {
                    continue;
                }
                $categories = $db->query("SELECT pc.category_id, c.url, c.position FROM s_products_categories pc LEFT JOIN s_categories c ON (c.id = pc.category_id) WHERE pc.product_id = $object->id ORDER BY pc.category_id;")->getWithObject();

                if (!empty($object->filename)) {
                    $src = "http://l-polyana.info/files/originals/" . $object->filename;
                    $link = Config::getUrlToReducer() . '?method=resizeImage&url=' . $src;
                } else {
                    $link = '';
                }

                foreach ($categories->fetchAll() as $category) {
                    $buf[$category->position][] = array(
                        'id'          => $object->id,
                        'src'         => $link . '&w=125&h=85',
                        'srcMin'      => $link . '&w=85&h=50',
                        'srcBig'      => $link . '&w=375&h=255',
                        'name'        => $object->name,
                        'parent'      => $category->url,
                        'description' => trim($object->annotation),
                        'amount'      => $object->price,
                        'amount2'     => 0,
                        'weight'      => $object->variant_name
                    );
                }
            }

            foreach ($buf as $item) {
                foreach ($item as $value) {
                    $data[] = $value;
                }
            }

            if (getPostGetCookie('script')) {
                echoJson($data, 'var ' . getPostGetCookie('script') . '=', ';');
            } else {
                echoJson($data);
            }
            break;
        case 'categories':
            $db = db::getInstance();
            $objects = $db->query("SELECT c.url AS id, (SELECT url FROM s_categories WHERE id = c.parent_id) AS parent, c.name, c.position, (SELECT COUNT(id) FROM s_categories WHERE id = c.parent_id) AS child_count FROM s_categories c WHERE id IN (SELECT id FROM s_products_categories) ORDER BY c.position;")->getWithObject();
            $data = [];
            while ($object = $objects->fetch()) {
                $data[] = [
                    'isTag'  => $object->parent == null && $object->child_count > 0,
                    'id'     => $object->id,
                    'name'   => $object->name,
                    'parent' => $object->parent ? $object->parent : 0,
                    'order'  => $object->position
                ];
            }
            if (getPostGetCookie('script')) {
                echoJson($data, 'var ' . getPostGetCookie('script') . '=', ';');
            } else {
                echoJson($data);
            }
            break;
        case 'page':
            $db = db::getInstance();
            $objects = $db->query("SELECT name,body FROM s_pages WHERE url = '" . getPostGetCookie('url') . "';")->getWithObject();
            $data = $objects->fetch();
            if (getPostGetCookie('script')) {
                echoJson($data, 'var ' . getPostGetCookie('script') . '=', ';');
            } else {
                echoJson($data);
            }
            break;
        case 'resizeImage':
            if (empty($_GET['url']) || empty($_GET['w']) || empty($_GET['h']))
                break;
            $ir = new ImageReducer($_GET['url'], $_GET['w'], $_GET['h']);
            $ir->showImage();
            $ir->close();
            break;
        case 'register':
            $phone = filter_input(INPUT_GET, 'phone', FILTER_SANITIZE_NUMBER_INT);
            if (!$phone || strlen($phone) <> 10) {
                echoJson('Неверно указан номер телефона');
                return;
            }
            $month = filter_input(INPUT_GET, 'month', FILTER_SANITIZE_NUMBER_INT);
            if (!$month) {
                echoJson('Неверно указана дата');
                return;
            }
            $day = filter_input(INPUT_GET, 'day', FILTER_SANITIZE_NUMBER_INT);
            if (!$day) {
                echoJson('Неверно указана дата');
                return;
            }
            $db = db::getInstance();
            $objects = $db->query("SELECT 1 FROM `users` WHERE `phone` = ?", array($phone))->getWithObject();
            if ($objects->fetch()) {
                getData('login');
                return;
            }
            $id = $db->query(
                "INSERT INTO `users` (`phone`,`bdate`) VALUES (?,?)",
                array($phone, json_encode(array($month, $day)))
            )->lastInsertId();

            if (getPostGetCookie('script'))
                echoJson(array(
                    'id'    => $id,
                    'phone' => $phone
                ), 'var ' . getPostGetCookie('script') . '=', ';');
            else
                echoJson(array(
                    'id'    => $id,
                    'phone' => $phone
                ));
            break;
        case 'login':
            $phone = filter_input(INPUT_GET, 'phone', FILTER_SANITIZE_NUMBER_INT);
            if (!$phone || strlen($phone) <> 10) {
                echoJson('Неверно указан номер телефона');
                return;
            }
            $db = db::getInstance();
            $objects = $db->query("SELECT * FROM `users` WHERE `phone` = ?", array($phone))->getWithObject();
            $object = $objects->fetch();
            if (!$object) {
                getData('register');
                return;
            }
            $latestOrder = new DateTime($object->latest_order);
            $date = new DateTime();
            if (getPostGetCookie('script'))
                echoJson(array(
                    'id'            => $object->id,
                    'phone'         => $object->phone,
                    'name'          => $object->name,
                    'latestOrder'   => $latestOrder->getTimestamp(),
                    'latestAddress' => $object->address_last_order,
                    'time'          => $date->getTimestamp(),
                    'bdate'         => json_decode($object->bdate)
                ), 'var ' . getPostGetCookie('script') . '=', ';');
            else
                echoJson(array(
                    'id'            => $object->id,
                    'phone'         => $object->phone,
                    'name'          => $object->name,
                    'latestOrder'   => $latestOrder->getTimestamp(),
                    'latestAddress' => $object->address_last_order,
                    'time'          => $date->getTimestamp(),
                    'bdate'         => json_decode($object->bdate)
                ));
            break;
        case 'buy':
            $data = json_decode(urldecode(base64_decode(getPostGetCookie('data'))));
            $objects = $data->items;
            if (
                count($objects) == 0
                || empty($data->data->name)
                || empty($data->data->phone)
                || empty($data->data->address)
            )
                return;

            $db = db::getInstance();
            $objects = $db->query("SELECT 1 FROM `users` WHERE `phone` = ?", array($data->data->phone))->getWithObject();
            if (!$objects->fetch())
                $db->query("INSERT INTO `users` (`phone`) VALUES (?)", array($data->data->phone))->lastInsertId();

            $db->query("UPDATE `users` SET `name`=?,`latest_order`=NOW(),`address_last_order`=? WHERE `phone`=?", array(
                $data->data->name,
                $data->data->address,
                $data->data->phone
            ));

            $objects = $db->query("SELECT p.id, p.url, p.name, p.annotation, (SELECT filename FROM s_images WHERE product_id = p.id AND NOT filename IS NULL LIMIT 1) AS filename, (SELECT variant_name FROM s_purchases WHERE p.id = s_purchases.product_id LIMIT 1) AS variant_name, (SELECT price FROM s_purchases WHERE p.id = s_purchases.product_id LIMIT 1) AS price FROM s_products p WHERE p.visible != 0 ORDER BY p.position;")->getWithObject();
            $dbData = array();
            while ($object = $objects->fetch()) {
                $dbData[$object->id] = array(
                    'name'    => $object->name,
                    'amount'  => $object->price,
                    'amount2' => $object->price
                );
            }

            $date = new DateTime($db->query("SELECT `latest_order` FROM `users` WHERE `phone`=?", array($data->data->phone))->getWithObject()->fetch()->latest_order);
            $date = $date->getTimestamp() + 1209600;
            $sale = ($date - now()) / 60 / 60 / 24 <= 14;

            //$sale = false;

            $message = '<h1>Форма клиента:</h1>'
                . '<table>'
                . '<tr><td>Имя: </td><td><b>' . $data->data->name . '</b></tr></tr>'
                . '<tr><td>Телефон: </td><td><b>' . $data->data->phone . '</b></tr></tr>'
                . '<tr><td>Адрес: </td><td><b>' . $data->data->address . '</b></tr></tr>'
                . '<tr><td>Коментарий: </td><td><b>' . $data->data->comment . '</b></tr></tr>'
                . ($sale ? '<tr><td>Скидка: </td><td><b>' . Config::getSale() . '%</b>(цена указана со скидкой)</tr></tr>' : '')
                . '</table>'
                . '<table style="text-align:center"><tr><th>Название</th><th>Цена</th><th>Кол-во</th><th>Общая цена</th></tr>';

            $amount = 0;
            foreach ($objects as $item) {
                if ($item->id == 'pizza') {
                    foreach ($item->data as $i) {
                        $a = $dbData[$i->id]['amount2'] / 4 * $i->amount;
                        if ($sale)
                            $a -= $a / 100 * Config::getSale();

                        $amount += $a;
                        $message .=
                            '<tr>'
                            . '<td>' . $dbData[$i->id]['name'] . '</td>'
                            . '<td>' . $dbData[$i->id]['amount2'] / 4 . ' руб.</td>'
                            . '<td style="padding-left:50px;padding-right:50px">' . $i->amount . ' кусочков пиццы</td>'
                            . '<td>' . $a . ' руб.</td>'
                            . '</tr>';
                    }
                } else {
                    $a = $dbData[$item->id]['amount'] * $item->amount;
                    if ($sale) {
                        $a = $a - $a / 100 * Config::getSale();
                    }

                    $amount += $a;
                    $message .=
                        '<tr>'
                        . '<td>' . $dbData[$item->id]['name'] . '</td>'
                        . '<td>' . $dbData[$item->id]['amount'] . ' руб.</td>'
                        . '<td style="padding-left:50px;padding-right:50px">' . $item->amount . '</td>'
                        . '<td>' . $a . ' руб.</td>'
                        . '</tr>';
                }
            }

            $message .= '<tr><td></td><td></td><td><b>Общая сумма:</b></td><td>' . $amount . ' руб.</td></tr>'
                . '</table>';

            if (getPostGetCookie('script')) {
                echoJson(array(mail(Config::getMailTo(), Config::getMailSubject(), $message, Config::getMailHeaders())), 'var ' . getPostGetCookie('script') . '=', ';');
            } else {
                echoJson(array(mail(Config::getMailTo(), Config::getMailSubject(), $message, Config::getMailHeaders())));
            }
            break;
        case 'bdate':
            echo 'false';
//            $phone = filter_input(INPUT_GET, 'phone', FILTER_SANITIZE_NUMBER_INT);
//            if (!$phone || strlen($phone) <> 10) {
//                echoJson('Неверно указан номер телефона');
//                return;
//            }
//            $db = db::getInstance();
//            $bdate = json_decode($db->query("SELECT `bdate` FROM `users` WHERE `phone`=?", array($phone))->getWithObject()->fetch()->bdate);
//            $date = new DateTime();
//            if ($bdate->day === (int)$date->format('d') && $bdate->month === ((int)$date->format('m')) + 1) {
//                echo 'true';
//            } else {
//                echo 'false';
//            }
            break;
        case 'reCacheImages':
            $db = db::getInstance();
            $objects = $db->query("SELECT filename FROM s_images")->getWithObject();
            while ($object = $objects->fetch()) {
                $src = "http://l-polyana.info/files/originals/" . $object->filename;

                $ir1 = new ImageReducer($src, 125, 85);
                unlink($ir1->getLocalPath());
                $ir1->cache();
                $ir1->close();

                $ir2 = new ImageReducer($src, 85, 50);
                unlink($ir2->getLocalPath());
                $ir2->cache();
                $ir2->close();

                $ir3 = new ImageReducer($src, 375, 255);
                unlink($ir3->getLocalPath());
                $ir3->cache();
                $ir3->close();
            }
            break;
        default:
            break;
    }
}
