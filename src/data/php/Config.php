<?php

/**
 * Created by PhpStorm.
 * User: RedCreepster
 * Date: 23.06.15
 * Time: 10:50
 */
class Config {
    private static $host = '127.0.0.1';
    private static $user = 'u0103121_lp_new';
    private static $db = 'u0103121_lp_new';
    private static $password = 'as5d465aa4s6d';

    private static $dirToCache = __DIR__ . '../../cache/';
    private static $urlToCacheDir = 'http://l-polyana.info/app/data/cache/';
    private static $urlToReducer = 'http://l-polyana.info/app/data/php/getData.php';

    private static $mailTo = 'polyana.zakaz@yandex.ru,79253133196@yandex.ru';
    private static $mailSubject = 'Заказаные через мобильное приложение товары';
    private static $mailHeaders = "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\nFrom: mobile-app@l-polyana.info\r\n";

    private static $sale = 10;

    /**
     * @return string
     */
    public static function getHost() {
        return self::$host;
    }

    /**
     * @return string
     */
    public static function getUser() {
        return self::$user;
    }

    /**
     * @return string
     */
    public static function getDb() {
        return self::$db;
    }

    /**
     * @return string
     */
    public static function getPassword() {
        return self::$password;
    }

    /**
     * @return array
     */
    public static function getData() {
        return array(
            'host'     => self::$host,
            'user'     => self::$user,
            'db'       => self::$db,
            'password' => self::$password
        );
    }

    /**
     * @return string
     */
    public static function getDirToCache() {
        return self::$dirToCache;
    }

    /**
     * @return string
     */
    public static function getUrlToCacheDir() {
        return self::$urlToCacheDir;
    }

    /**
     * @return string
     */
    public static function getUrlToReducer() {
        return self::$urlToReducer;
    }

    /**
     * @return string
     */
    public static function getMailTo() {
        return self::$mailTo;
    }

    /**
     * @return string
     */
    public static function getMailSubject() {
        return self::$mailSubject;
    }

    /**
     * @return string
     */
    public static function getMailHeaders() {
        return self::$mailHeaders;
    }

    /**
     * @return int
     */
    public static function getSale() {
        return self::$sale;
    }

}