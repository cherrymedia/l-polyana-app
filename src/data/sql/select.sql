SELECT
  c.url AS id,
  (
    SELECT url
    FROM s_categories
    WHERE id = c.parent_id
  )     AS parent,
  c.name,
  c.position,
  (
    SELECT COUNT(id)
    FROM s_categories
    WHERE id = c.parent_id
  )     AS child_count
FROM s_categories c
WHERE id IN (
  SELECT id
  FROM s_products_categories
)
ORDER BY c.position;

SELECT
  p.url AS id,
  p.name,
  p.annotation,
  c.url AS category_id,
  i.filename,
  c.position,
  pr.variant_name,
  pr.price,
  c.position
FROM s_products p
  LEFT JOIN s_products_categories pc ON (p.id = pc.product_id)
  LEFT JOIN s_categories c ON (c.id = pc.category_id)
  LEFT JOIN s_images i ON (p.id = i.product_id)
  LEFT JOIN s_purchases pr ON (p.id = pr.product_id)
WHERE p.visible = 1
ORDER BY c.position;